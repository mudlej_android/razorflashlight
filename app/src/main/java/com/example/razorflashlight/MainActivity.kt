package com.example.razorflashlight

import android.annotation.SuppressLint
import android.content.Context
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraManager
import android.os.Bundle
import android.view.MotionEvent
import androidx.appcompat.app.AppCompatActivity
import com.example.razorflashlight.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var camManager: CameraManager
    private var cameraId: String? = null
    private var cameraMode: Boolean = false

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        camManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            cameraId = camManager.cameraIdList[0]
            binding.toggleButton.setOnClickListener {
                toggleFlashLight( )
            }
            binding.holdDownButton.setOnTouchListener { view, motionEvent ->
                when (motionEvent.action){
                    MotionEvent.ACTION_DOWN -> setFlashLight(true)
                    MotionEvent.ACTION_UP -> setFlashLight(false)
                }
                true
            }
        }
        catch (error: CameraAccessException) {
            Snackbar.make(binding.root, "Can't access flashlight", Snackbar.LENGTH_INDEFINITE).show()
            binding.holdDownButton.isEnabled = false
            binding.toggleButton.isEnabled = false
        }
    }

    private fun toggleFlashLight() {
        cameraMode = !cameraMode
        camManager.setTorchMode(cameraId!!, cameraMode)
    }

    private fun setFlashLight(mode: Boolean) {
        camManager.setTorchMode(cameraId!!, mode)
    }

    //fun hasFlash() = packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
}